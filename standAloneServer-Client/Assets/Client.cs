﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System;
using UnityEngine.UI;


public class Client : MonoBehaviour {

	private bool socketReady;
	private TcpClient socket;
	private NetworkStream stream;
	private StreamWriter writer;
	private StreamReader reader;
	public Text feedback;
	public Text input;
	
	
	public void connectToServer()
	{
		
		
		if (socketReady)
			return;
		
		//default host / port values
		string host = "127.0.0.1";
		int port = 6321;
		
		if (input.text != "" && input.text != null)
			host = input.text;
			
		Debug.Log("Connecting to " + host + " on port " + port.ToString());
		
		try
		{
			socket = new TcpClient(host, port);
			stream = socket.GetStream();
			writer = new StreamWriter(stream);
			reader = new StreamReader(stream);
			socketReady = true;
		}
		catch(Exception e)
		{
			Debug.Log("Socket error: " + e.Message);
			feedback.text = "Socket error: " + e.Message;
		}
	
	}
	
	
	
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(socketReady)
		{
			if (stream.DataAvailable)
			{
				string data = reader.ReadLine();
				if (data != null)
					OnIncomingData(data);
			}
		}
		
	}
	
	private void OnIncomingData(string data)
	{
		
		Debug.Log("Server says: " + data);
		feedback.text = "Server says: " + data;
	}
}
