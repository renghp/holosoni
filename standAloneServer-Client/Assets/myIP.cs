﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Linq;

public class myIP : MonoBehaviour {
	
	//public Text feedback;
	public Text feedback2;

	// Use this for initialization
	void Start () {
	
	

	
	}
	
	
	// Update is called once per frame
	void Update () {
		

		feedback2.text = "Local WIFI IP: \n" + GetLocalIPv4();
		
	}
	

	public string GetLocalIPv4()
     {
         return Dns.GetHostEntry(Dns.GetHostName()).AddressList.First( f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
     }
}