﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System;
using System.Net;
using System.IO;

public class Server : MonoBehaviour {

    public int port = 6321;
    private List<ServerClient> holoclients;
    private TcpListener androidServer;
    private bool serverStarted;
	public Text feedback;
	string serverMessage = "No new messages";
	
	public Text target;
    public Text method;
    public Text tutorial;

	// Use this for initialization
	void Start () {

        holoclients = new List<ServerClient>();

        try
        {
            androidServer = new TcpListener(IPAddress.Any, port);
            androidServer.Start();

            StartListening();
            serverStarted = true;
			
			serverMessage = "Server ready. No new messages";
            Debug.Log("server started on port " + port.ToString());     //tá caindo sempre aqui, mesmo com porta 1, 23, 80...
			
			
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e.Message);
			serverMessage = "Socket error: " + e.Message;
        }
	}



    // Update is called once per frame
    void Update () {
		
		feedback.text = serverMessage;

        if (!serverStarted)
            return;

        foreach (ServerClient c in holoclients)
        {
            if (!IsConnected(c.tcp))
            {
                c.tcp.Close();
                continue;
            }
            else
            {
                NetworkStream s = c.tcp.GetStream();
                if (s.DataAvailable)
                {
                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();

                    if (data != null)
                        OnIncomingData(c, data);
                }
            }
        }

        string format = "hh-mm-ss dd MMMM yy";

        string broadcastMessage = target.text + "|" + method.text + "|" + tutorial.text; //+ "|" + System.DateTime.Now.ToString(format);
		
		Broadcast(broadcastMessage, holoclients);

        Debug.Log("broadcasting " + broadcastMessage);
	}



    private void StartListening()
    {
        androidServer.BeginAcceptTcpClient(AcceptTcpClient, androidServer);
		Debug.Log("rodando1");
    }

    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                {
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
                }

                return true;
            }
            else
                return false;
        }
        catch
        {
            return false;

        }
    }

    private void AcceptTcpClient(IAsyncResult ar)
    {
        TcpListener listener = (TcpListener)ar.AsyncState;

        holoclients.Add(new ServerClient(listener.EndAcceptTcpClient(ar)));
		Debug.Log("rodando2");
        StartListening();
		Debug.Log("rodando3");

        //send a message here 
		serverMessage = "I say: " + holoclients[holoclients.Count-1].clientName + " has connected";
		Broadcast(holoclients[holoclients.Count-1].clientName + " has connected", holoclients);
		
		//Broadcast(gpsSignal.text, holoclients);

    }
	
	private void Broadcast(string data, List<ServerClient> cl)
	{
		foreach(ServerClient c in cl)
		{
			try
			{
				StreamWriter writer = new StreamWriter(c.tcp.GetStream());
				writer.WriteLine(data);
				writer.Flush();
			}
			catch(Exception e)
			{
				Debug.Log("Writting error: " + e.Message + " to client " + c.clientName);
			}
		}
	}

    private void OnIncomingData(ServerClient c, string data)
    {
        Debug.Log(c.clientName + " sent " + data);
        serverMessage = c.clientName + " sent " + data;
    }
}

public class ServerClient
{
    public TcpClient tcp;
    public string clientName;
    public ServerClient(TcpClient clientSocket)
    {
        clientName = "Hologuest";
        tcp = clientSocket;
    }
}
