using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followCamera : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform camera;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        transform.position = camera.position;
        transform.eulerAngles = camera.eulerAngles;
        
    }
}
