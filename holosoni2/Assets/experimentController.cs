using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Globalization;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.Audio;
using Microsoft.MixedReality.Toolkit.Examples.Demos;

public class experimentController : MonoBehaviour
{
    // Start is called before the first frame update

    string fileName, path;

    public GameObject[] targetPosition; //  a1 0 |   a2 1   |   a3 2    |           | a5 3  |           //vertical disambiguation test: accesses all 19
                                        //  b1 4 |   b2 5   |   b3 6    |  b4 7     | b5 8  |           //base experiment: accesses 4 - 18
                                        //  c1 9 |   c2 10  |   c3 11   |  c4 12    | c5 13 |
                                        //       |          |           |           | d5 14 |   d6 15
                                        //       |          |           |  e4 16    | e5 17 |   e6 18


    public GameObject[] methodBeacon;   // MIXED Relative 0 | MIXED Absolute 1 | MIXED fixed 2 - 6
                                        // HAND Relative 7 | HAND Absolute 8 | HAND fixed 9 - 13
                                        // HEAD Relative 14 | HEAD Absolute 15 | HEAD fixed 16 - 20
    public int trialNumber, methodNumber;

    public Text serverFeedback;

    string[] splitServerFeedback;
    string currentMethod, currentTarget, currentTutorial;    // mixed Relative 0 | mixed Absolute 1 | mixed fixed 2 | stop sounds 404
    string lastTimeStamp;
    
    public GameObject introTutorialController, ABSTutorialController, RELTutorialController, FIXTutorialController;

    float timer = 0.0f;

    public Transform userCamera;

    public Transform userHand;

    Vector3 lastUserCameraPosition;
    Vector3 lastUserCameraRotation;

    float distanceTravelled = 0.0f;
    float YdistanceTravelled = 0.0f;

    float XrotationTravelled = 0.0f;
    float YrotationTravelled = 0.0f;
    float ZrotationTravelled = 0.0f;

    Vector3 lastHandTransform;

    float handDistanceTravelled = 0.0f;
    float handYdistanceTravelled = 0.0f;


    void Start()
    {
       
        lastUserCameraPosition = userCamera.position;
        lastUserCameraRotation = userCamera.localEulerAngles;

        lastHandTransform.x = 0f;
        lastHandTransform.y = 0f;
        lastHandTransform.z = 0f;

        string format = "hh-mm-ss dd MMMM yy";    //

        fileName = System.DateTime.Now.ToString(format) + ".txt";


        path = Path.Combine(Application.persistentDataPath, fileName);   //writes to LocalAppData/YourApp/LocalState

        
        fileWriter("starting " + "\n");

        stopAllSounds(false);
    }

    // Update is called once per frame
    void Update()
    {

        distanceTravelled += Vector2.Distance (new Vector2 (userCamera.position.x, userCamera.position.z), new Vector2 (lastUserCameraPosition.x, lastUserCameraPosition.z));
        YdistanceTravelled += Math.Abs(userCamera.position.y - lastUserCameraPosition.y);

        float Xrotation = Math.Abs(userCamera.localEulerAngles.x - lastUserCameraRotation.x);
        float Yrotation = Math.Abs(userCamera.localEulerAngles.y - lastUserCameraRotation.y);
        float Zrotation = Math.Abs(userCamera.localEulerAngles.z - lastUserCameraRotation.z);

        try
        {
            userHand = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

            //Debug.Log("right hand found at " + userHand.position.x + " " + userHand.position.y + " " + userHand.position.z);
            
        }
        catch (Exception e)
        {
            try
            {
                userHand = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand
                //Debug.Log("left hand found at " + userHand.position.x + " " + userHand.position.y + " " + userHand.position.z);
               
                
            }
            catch (Exception e2)                                    //if no hands, disable soni
            {
                userHand = null;
            }
        }

        
        
        if (userHand != null && lastHandTransform.x != 0f && lastHandTransform.y != 0f && lastHandTransform.z != 0f)
        {
            

            handDistanceTravelled += Vector2.Distance (new Vector2 (userHand.position.x, userHand.position.z), new Vector2 (lastHandTransform.x, lastHandTransform.z));
            handYdistanceTravelled += Math.Abs(userHand.position.y - lastHandTransform.y);
            //Debug.Log("handYdistanceTravelled =  " + userHand.position.y + " - " + lastHandTransform.y + " = " + handYdistanceTravelled);

            //Debug.Log("distance is now = " + handDistanceTravelled + " and " + handYdistanceTravelled);

            lastHandTransform.x = userHand.position.x;
            lastHandTransform.y = userHand.position.y;
            lastHandTransform.z = userHand.position.z;
        }
        else if (userHand != null)
        {
            lastHandTransform.x = userHand.position.x;
            lastHandTransform.y = userHand.position.y;
            lastHandTransform.z = userHand.position.z;
            //Debug.Log("hand found for the first time");
        }


        if (Xrotation < 170)
            XrotationTravelled += Xrotation;

        if (Yrotation < 170)  
            YrotationTravelled += Yrotation;

        if (Zrotation < 170)    
            ZrotationTravelled += Zrotation;

        timer += Time.deltaTime;

        lastUserCameraPosition = userCamera.position;
        lastUserCameraRotation = userCamera.localEulerAngles;

        try{
            splitServerFeedback = serverFeedback.text.Split('|');
            
            //Debug.Log("received: '" + serverFeedback.text + "'");

            if (currentTarget != splitServerFeedback[0] || currentMethod != splitServerFeedback[1])
            {
                //Debug.Log("new target or method");

                currentTarget = splitServerFeedback[0];
                currentMethod = splitServerFeedback[1];

                try{
                    //stopAllSounds(false);
                    int methodNumber = Int32.Parse(currentMethod);
                    int targetNumber = Int32.Parse(currentTarget);

                   // repositionBeacon(methodBeacon[methodNumber], targetPosition[targetNumber]);

                    if (methodNumber == 404 || targetNumber == 404)
                        stopAllSounds(true);
                    else
                        stopAllSounds(false);

                    //Debug.Log("will change target to " + targetNumber + "and method to " + methodNumber);

                    if (methodNumber != 2 && methodNumber != 9)
                    {
                        //Debug.Log("trying to change target to " + targetPosition[targetNumber] + " and method to " + methodBeacon[methodNumber]);

                        methodBeacon[methodNumber].transform.position = targetPosition[targetNumber].transform.position;

                        methodBeacon[methodNumber].SetActive(true);

                        repositionBeacon(methodBeacon[methodNumber], targetPosition[targetNumber]);

                        methodBeacon[methodNumber].GetComponent<AudioSource>().Play();

                        //Debug.Log("changed target to " + targetPosition[targetNumber] + " and method to " + methodBeacon[methodNumber]);
                    }
                    else if (methodNumber == 2)
                    {
                        if (targetNumber <= 3)  //a1 - a5 - highest shelf
                            methodNumber = 6;   //highest pitch
                        else if (targetNumber <= 8) //b1 - b5 - high shelf
                             methodNumber = 5;   //high pitch
                        else if (targetNumber <= 13) //c1 - c5 - middle shelf
                            methodNumber = 4; // middle pitch
                        else if (targetNumber <= 15) //d5 - c6 - low shelf
                            methodNumber = 3; // low pitch  
                        else if (targetNumber <= 18) //e4 - e6 - lowest shelf
                            methodNumber = 2; // lowest pitch 


                        methodBeacon[methodNumber].transform.position = targetPosition[targetNumber].transform.position;
                        methodBeacon[methodNumber].SetActive(true);
                        repositionBeacon(methodBeacon[methodNumber], targetPosition[targetNumber]);
                        methodBeacon[methodNumber].GetComponent<AudioSource>().Play();
                    }
                     else if (methodNumber == 9)
                    {


                        if (targetNumber <= 3)  //a1 - a5 - highest shelf
                            methodNumber = 13;   //highest pitch
                        else if (targetNumber <= 8) //b1 - b5 - high shelf
                            methodNumber = 12;   //high pitch
                        else if (targetNumber <= 13) //c1 - c5 - middle shelf
                            methodNumber = 11; // middle pitch
                        else if (targetNumber <= 15) //d5 - c6 - low shelf
                            methodNumber = 10; // low pitch  
                        else if (targetNumber <= 18) //e4 - e6 - lowest shelf
                            methodNumber = 9; // lowest pitch 
                        //else 
                            //stopAllSounds(false);
                        


                        methodBeacon[methodNumber].transform.position = targetPosition[targetNumber].transform.position;

                        methodBeacon[methodNumber].SetActive(true);

                        repositionBeacon(methodBeacon[methodNumber], targetPosition[targetNumber]);
                       
                        methodBeacon[methodNumber].GetComponent<AudioSource>().Play();
                     
                    }
                    //else 
                        //stopAllSounds(false);


                    //stopAllSounds(false);

                    
                }
                catch {

                    //Debug.Log("something is afoot");
                };

            }

            try{
                    if (lastTimeStamp != splitServerFeedback[3])
                    {
                        
                        //Debug.Log("new time stamp detected is" + splitServerFeedback[3]);

                        lastTimeStamp = splitServerFeedback[3];
                        //if (currentTutorial != splitServerFeedback[2])
                        //{
                        currentTutorial = splitServerFeedback[2];
                        
                        

                        try{
                            int tutorialNumber = Int32.Parse(currentTutorial);

                            switch (tutorialNumber)
                                {
                                    
                                    case 0:     //previous INTRO tutorial
                                        introTutorialController.GetComponent<TextToSpeechSample>().PreviousSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 1:     //start INTRO tutorial
                                        introTutorialController.GetComponent<TextToSpeechSample>().TutorialSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 2:     //repeat current INTRO tutorial
                                        introTutorialController.GetComponent<TextToSpeechSample>().RepeatSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 3:     //next INTRO tutorial
                                        introTutorialController.GetComponent<TextToSpeechSample>().NextSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 10:     //previous REL tutorial
                                        RELTutorialController.GetComponent<TextToSpeechSample>().PreviousSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 11:     //start REL tutorial
                                        RELTutorialController.GetComponent<TextToSpeechSample>().TutorialSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 12:     //repeat REL INTRO tutorial
                                        RELTutorialController.GetComponent<TextToSpeechSample>().RepeatSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 13:     //next REL tutorial
                                        RELTutorialController.GetComponent<TextToSpeechSample>().NextSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 20:     //previous ABS tutorial
                                        ABSTutorialController.GetComponent<TextToSpeechSample>().PreviousSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 21:     //start ABS tutorial
                                        ABSTutorialController.GetComponent<TextToSpeechSample>().TutorialSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 22:     //repeat current ABS tutorial
                                        ABSTutorialController.GetComponent<TextToSpeechSample>().RepeatSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 23:     //next ABS tutorial
                                        ABSTutorialController.GetComponent<TextToSpeechSample>().NextSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 30:     //previous FIX tutorial
                                        FIXTutorialController.GetComponent<TextToSpeechSample>().PreviousSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 31:     //start FIX tutorial
                                        FIXTutorialController.GetComponent<TextToSpeechSample>().TutorialSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 32:     //repeat current FIX tutorial
                                        FIXTutorialController.GetComponent<TextToSpeechSample>().RepeatSpeak();
                                        //currentTutorial = "404";
                                        break;
                                    case 33:     //next FIX tutorial
                                        FIXTutorialController.GetComponent<TextToSpeechSample>().NextSpeak();
                                        //currentTutorial = "404";
                                        break;

                                    case 404:
                                    default:
                                        stopTutorialSounds();

                                        break;

                                }

                        }
                        catch {};
                        

                    }
            }
            catch{};



            
        }
        catch {};


    }

    void stopTutorialSounds()
    {
        //Debug.Log("stopping tutorial");

        foreach (GameObject mb in methodBeacon)
        {
            if (mb.tag == "tutorial")
            {
                try{
                    mb.GetComponent<AudioSource>().Stop();
                }
                catch {
                    //Debug.Log ("could not stop tutorial " + mb);
                };
            }
        }
    }

    void stopAllSounds(bool commandFromServer)
    {
        //Debug.Log("stopping method");
        
        if (commandFromServer)
        {

            try{

                fileWriter("elapsed time: " + timer);
                fileWriter("HEAD 2D/planar distance travelled: " + distanceTravelled);
                fileWriter("HEAD vertical distance travelled: " + YdistanceTravelled);
                fileWriter("HEAD X rotation turned: " + XrotationTravelled);
                fileWriter("HEAD Y rotation turned: " + YrotationTravelled);
                fileWriter("HEAD Z rotation turned: " + ZrotationTravelled);
                fileWriter("\n");
                fileWriter("HAND 2D/planar distance travelled: " + handDistanceTravelled);
                fileWriter("HAND vertical distance travelled: " + handYdistanceTravelled);
                fileWriter("////////////////////////////////////////////////////");

            }
            catch{
                //Debug.Log("something went wrong");
            }
        }

        timer = 0.0f;
        distanceTravelled = 0.0f;
        YdistanceTravelled = 0.0f;
        XrotationTravelled = 0.0f;
        YrotationTravelled = 0.0f;
        ZrotationTravelled = 0.0f;

        handDistanceTravelled = 0.0f;
        handYdistanceTravelled = 0.0f;

        foreach (GameObject mb in methodBeacon)
        {
            if (mb.tag != "tutorial")
            {
                try{
                    mb.GetComponent<AudioSource>().Stop();
                    

                    if (mb.tag != "pseudo")
                        mb.SetActive(false);
                }
                catch {
                    //Debug.Log ("could not stop method " + mb);
                };
            }
        }
    }

    void fileWriter(string dataToWrite)
    {
        try{
                File.AppendAllText(path, dataToWrite + "\n");
                //Debug.Log("written to " + path);
            }
        catch{
                    //Debug.Log ("could not write " + dataToWrite + " to file");
                };

    }

    void repositionBeacon(GameObject beacon, GameObject newTargetPosition)
    {
        //Debug.Log("logging");
        //Debug.Log("logging change for beacon " + beacon.tag + " and target " + newTargetPosition.name);
        fileWriter("beacon " + beacon.tag + " repositioned to " + newTargetPosition.name);
       // Debug.Log("logged");

    }
}
