﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System;
using System.Net;
using System.IO;
using System.Net.NetworkInformation;
using System.Linq;






public class Server : MonoBehaviour {

    //public int port = 6321;
    private List<ServerClient> holoclients;
    private TcpListener androidServer;
    private bool serverStarted;
	public Text feedback;
	string serverMessage = "No new messages";
	
	public Text target;
    public Text method;
    public Text tutorial;
    public Text controllerPos;
    public Transform playerCameraFollower;
    public Transform ARplayerCameraFollower, ARrightHandFollower, ARleftHandFollower;

    private String[] splitARFeedback, splitARPos;

    public Text feedback2;


    //private float startTime;


	// Use this for initialization
	void Start () {

        holoclients = new List<ServerClient>();

       // startTime = Time.time;

        try
        {
            androidServer = new TcpListener(IPAddress.Any, 0);
            androidServer.Start();

            StartListening();
            serverStarted = true;
			
			serverMessage = "Server ready on port "+ ((IPEndPoint)androidServer.LocalEndpoint).Port.ToString() +". No new messages";
            //Debug.Log("server started on port " + port.ToString());     //tá caindo sempre aqui, mesmo com porta 1, 23, 80...
			
			
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e.Message);
			serverMessage = "Socket error: " + e.Message;
        }
	}



    // Update is called once per frame
    void Update () {
		
		feedback.text = serverMessage;

        feedback2.text = "Local WIFI IP: \n" + GetLocalIPv4() + ":" + ((IPEndPoint)androidServer.LocalEndpoint).Port.ToString();
		

        if (!serverStarted)
            return;

        foreach (ServerClient c in holoclients)
        {
            if (!IsConnected(c.tcp))
            {
                c.tcp.Close();
                continue;
            }
            else
            {
                NetworkStream s = c.tcp.GetStream();
                if (s.DataAvailable)
                {
                    StreamReader reader = new StreamReader(s, true);
                    string data = reader.ReadLine();

                    if (data != null)
                        OnIncomingData(c, data);
                }
            }
        }

        string format = "hh-mm-ss dd MMMM yy";


        string broadcastMessage = target.text + "|" + method.text + "|" + tutorial.text + "|" + controllerPos.text + "|" + playerCameraFollower.localPosition.x + "$" + playerCameraFollower.localPosition.y + "$" + playerCameraFollower.localPosition.z + "$" + playerCameraFollower.localEulerAngles.x + "$" + playerCameraFollower.localEulerAngles.y + "$" + playerCameraFollower.localEulerAngles.z + "|" + System.DateTime.Now.ToString(format);
		
		Broadcast(broadcastMessage, holoclients);

        //Debug.Log("broadcasting " + broadcastMessage);
	}


    public string GetLocalIPv4()
     {
         return Dns.GetHostEntry(Dns.GetHostName()).AddressList.First( f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
     }

    private void StartListening()
    {
        androidServer.BeginAcceptTcpClient(AcceptTcpClient, androidServer);
		//Debug.Log("rodando1");
    }

    private bool IsConnected(TcpClient c)
    {
        try
        {
            if (c != null && c.Client != null && c.Client.Connected)
            {
                if (c.Client.Poll(0, SelectMode.SelectRead))
                {
                    return !(c.Client.Receive(new byte[1], SocketFlags.Peek) == 0);
                }

                return true;
            }
            else
                return false;
        }
        catch
        {
            return false;

        }
    }

    private void AcceptTcpClient(IAsyncResult ar)
    {
        TcpListener listener = (TcpListener)ar.AsyncState;

        holoclients.Add(new ServerClient(listener.EndAcceptTcpClient(ar)));
		//Debug.Log("rodando2");
        StartListening();
		//Debug.Log("rodando3");

        //send a message here 
		serverMessage = "I say: " + holoclients[holoclients.Count-1].clientName + " has connected";
		Broadcast(holoclients[holoclients.Count-1].clientName + " has connected", holoclients);
		
		//Broadcast(gpsSignal.text, holoclients);

    }
	
	private void Broadcast(string data, List<ServerClient> cl)
	{
		foreach(ServerClient c in cl)
		{
			try
			{
				StreamWriter writer = new StreamWriter(c.tcp.GetStream());
				writer.WriteLine(data);
				writer.Flush();
			}
			catch(Exception e)
			{
				Debug.Log("Writting error: " + e.Message + " to client " + c.clientName);
			}
		}
	}

    private void OnIncomingData(ServerClient c, string data)
    {
        //Debug.Log(c.clientName + " sent " + data);
        serverMessage = c.clientName + " sent " + data;

        splitARFeedback = data.Split('#');

        splitARPos = splitARFeedback[0].Split('$');

        Vector3 pos = new Vector3 (float.Parse(splitARPos[0]), float.Parse(splitARPos[1]), float.Parse(splitARPos[2]));

        Vector3 rot = new Vector3 (float.Parse(splitARPos[3]), float.Parse(splitARPos[4]), float.Parse(splitARPos[5]));

       /* float distCovered = (Time.time - startTime) * 1f;

        float journeyLength = Vector3.Distance(ARplayerCameraFollower.localPosition, pos);

        // Fraction of journey completed equals current distance divided by total distance.
        float fractionOfJourney = distCovered / journeyLength;

        */

       // float journeyLength = Vector3.Distance(ARplayerCameraFollower.localPosition, pos);      //helps with random dislocations, but not perfect, also may get you stuck

       // if (journeyLength < 1 )
            ARplayerCameraFollower.localPosition =  pos; // Vector3.Lerp(ARplayerCameraFollower.localPosition, pos, fractionOfJourney);

      //  float xDif = Math.Abs(ARplayerCameraFollower.localEulerAngles.x - rot.x);               //helps with random angle dislocations, but not perfect, also may get you stuck
     //   float yDif = Math.Abs(ARplayerCameraFollower.localEulerAngles.y - rot.y);
      //  float zDif = Math.Abs(ARplayerCameraFollower.localEulerAngles.z - rot.z);

       // if (xDif < 90 && yDif < 90 && zDif < 90)
            ARplayerCameraFollower.localEulerAngles = rot; //Vector3.Lerp(ARplayerCameraFollower.localEulerAngles, rot, 1f);

        splitARPos = splitARFeedback[1].Split('$');

        ARrightHandFollower.localPosition = new Vector3 (float.Parse(splitARPos[0]), float.Parse(splitARPos[1]), float.Parse(splitARPos[2]));
        ARrightHandFollower.localEulerAngles = new Vector3 (float.Parse(splitARPos[3]), float.Parse(splitARPos[4]), float.Parse(splitARPos[5]));

        splitARPos = splitARFeedback[2].Split('$');

        ARleftHandFollower.localPosition = new Vector3 (float.Parse(splitARPos[0]), float.Parse(splitARPos[1]), float.Parse(splitARPos[2]));
        ARleftHandFollower.localEulerAngles = new Vector3 (float.Parse(splitARPos[3]), float.Parse(splitARPos[4]), float.Parse(splitARPos[5]));
    }
}

public class ServerClient
{
    public TcpClient tcp;
    public string clientName;
    public ServerClient(TcpClient clientSocket)
    {
        clientName = "Hologuest";
        tcp = clientSocket;
    }
}
