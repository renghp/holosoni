﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Valve.VR;

//using UnityEngine.InputSystem;

public class controllerController : MonoBehaviour
{

    public SteamVR_Action_Boolean TriggerClick;
    private SteamVR_Input_Sources inputSource;

    public Text targetText;
	public Text destField;

	public Text methodText;
    public Text tutorialText;

    public Text controllerPosition;

    public Transform rightCont;
    public Transform target;

    public bool targetIsOn = false;

    

    void Start()
    {
		
        targetText.text = "Waiting for target command";
        tutorialText.text = "waiting for tutorial command";// + "|" + System.DateTime.Now.ToString() + "|";
        
    }


	
	void Update()
    {



        if (Input.GetKeyDown("p"))
        {
            //Debug.Log("pressed");

            targetPositionFromController();

        }

        

		if (destField.text != "")
			targetText.text = destField.text;



		

    }

    private void OnEnable()
    {
        TriggerClick.AddOnStateDownListener(Press, inputSource);
    }
 
    private void OnDisable()
    {
        TriggerClick.RemoveOnStateDownListener(Press, inputSource);
    }
 
    private void Press(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        //put your stuff here
        //print("Success");

        targetPositionFromController();
    }
 
 



   /* public void Fire(InputAction.CallbackContext context)
    {
        Debug.Log("Fire!");
    }*/

    public void targetPositionFromController()
	{
        
       // Debug.Log("setting target");    

        if (!targetIsOn)
        {
            targetIsOn = true;

            target.gameObject.SetActive(true);

            target.position =  rightCont.position;

            

            controllerPosition.text = target.localPosition.x + "#" + target.localPosition.y + "#" + target.localPosition.z;     //changes broadcast text to be target position

            methodTextFromButton("8");      //turns on ABS sound on AR end
            targetTextFromButton("19");
        }
        else
        {
            targetIsOn = false;

            target.gameObject.SetActive(false);

            methodTextFromButton("404");        //these deactivate sound on AR end
            targetTextFromButton("404");

        }

        //Debug.Log("target set");

	}

	public void targetTextFromButton(string btText)
	{
        //Debug.Log("pressed");
		targetText.text = btText;

        

	}

	public void methodTextFromButton(string btText)
	{
		methodText.text = btText;
    
	}

    public void tutorialTextFromButton(string btText)
	{
        if (methodText.text != "7" && methodText.text != "8" && methodText.text != "9")
        {
            tutorialText.text = btText + "|" + System.DateTime.Now.ToString() + "|";

            //Debug.Log("NO method pressed");

        }
		    
        else if (methodText.text == "7")
        {
            tutorialText.text = "1" + btText + "|" + System.DateTime.Now.ToString() + "|";
            //Debug.Log("method 7 pressed");
        }
        else if (methodText.text == "8")
        {
            tutorialText.text = "2" + btText + "|" + System.DateTime.Now.ToString() + "|";
            //Debug.Log("method 8 pressed");
        }
        else if (methodText.text == "9")
        {
            tutorialText.text = "3" + btText + "|" + System.DateTime.Now.ToString() + "|";
            //Debug.Log("method 9 pressed");
        }
	}

}

/*using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gpsSignal : MonoBehaviour
{
	public bool gpsIsOn = false;
	public Text gpsText;
	
	
	 void Start()
    {
		Input.location.Start();
		 StartCoroutine(getGPS());
	}
	
    IEnumerator getGPS()
    {
		Debug.Log("1");
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
			gpsText.text = "Timed out";
            Debug.Log("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
			gpsText.text ="Unable to determine device location";
            Debug.Log("Unable to determine device location");
            yield break;
        }
        else
        {
            gpsIsOn = true;

            while (gpsIsOn)
            { 
                //we can change the boolean to false from another function or here in an if statement
			    gpsText.text = "Lat: " + Input.location.lastData.latitude + " Lon: " + Input.location.lastData.longitude + " Alt: " + Input.location.lastData.altitude + " horizontal acc: " + Input.location.lastData.horizontalAccuracy + "TS: " + Input.location.lastData.timestamp;
			    
			    //Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

            }

        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }
	
	void Update()
	{
		

            
	}
}*/
