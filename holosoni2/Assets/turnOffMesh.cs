using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class turnOffMesh : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject mesh;
    void Start()
    {
        mesh.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void turnMeshOnOff(bool meshStatus)
    {
        mesh.SetActive(meshStatus);


        string path = Path.Combine(Application.persistentDataPath, "meshStatus.txt");   //writes to LocalAppData/YourApp/LocalState
        using (TextWriter writer = File.CreateText(path))
        {
            writer.WriteLine("current mesh status is: " + meshStatus);
        }

    }


}
