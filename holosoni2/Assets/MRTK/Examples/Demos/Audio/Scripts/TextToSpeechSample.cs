// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using Microsoft.MixedReality.Toolkit.Audio;
using UnityEngine;

namespace Microsoft.MixedReality.Toolkit.Examples.Demos
{
    /// <summary>
    /// Sample script for text to speech
    /// </summary>
    [RequireComponent(typeof(TextToSpeech))]
    public class TextToSpeechSample : MonoBehaviour
    {
        private TextToSpeech textToSpeech;

        public string messageToBeRead;

        public string [] messagesToBeRead;
        private int messageCounter = 0;

        public AudioClip [] nonStandardMessages;



        private void Awake()
        {
            textToSpeech = GetComponent<TextToSpeech>();
        }

        /// <summary>
        /// Start playing Text To Speech generated voice
        /// </summary>
        public void Speak()
        {

            //Debug.Log("started speaking");
            // If we have a text to speech manager on the target object, say something.
            // This voice will appear to emanate from the object.
            if (textToSpeech != null)
            {
                // Create message
                // var msg = string.Format( "This is the {0} voice. It should sound like it's coming from the object you clicked. Feel free to walk around and listen from different angles.", textToSpeech.Voice.ToString());
                var msg = messageToBeRead;

                // Speak message
                textToSpeech.StartSpeaking(msg);
            }
        }

        private void sayMessage(int messageID)
        {

            if (textToSpeech != null)
            {
                
                var msg = messagesToBeRead[messageID];

                if (messageID != 4 && messageID != 5 && messageID != 7 && messageID != 8 && messageID != 12 && messageID != 13)
                    textToSpeech.StartSpeaking(msg);
                else if (messageID == 4)    //play FAST tempo sound afterwards
                {
                    gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[0];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (messageID == 5)    //play SLOW tempo sound afterwards
                {
                    gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[1];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (messageID == 7)    //play LEFT EAR sound afterwards
                {
                    gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[2];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (messageID == 8)    //play RIGHT EAR sound afterwards
                {
                    gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[3];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (messageID == 12)    //play LOW PITCH sound afterwards
                {
                   gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[4];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else if (messageID == 13)    //play HIGH PITCH sound afterwards
                {
                    gameObject.GetComponent<AudioSource>().clip = nonStandardMessages[5];
                    
                    gameObject.GetComponent<AudioSource>().Play();
                }
                else
                    textToSpeech.StartSpeaking(msg);
            }
        }

        public void TutorialSpeak()
        {

            messageCounter = 0;

            sayMessage(messageCounter);
        }

        public void NextSpeak()
        {
            if (messageCounter < messagesToBeRead.Length-1)
                messageCounter++;


            sayMessage(messageCounter);
        }

        public void RepeatSpeak()
        {

            sayMessage(messageCounter);
        }

        public void PreviousSpeak()
        {
            if (messageCounter != 0)
                messageCounter--;

            sayMessage(messageCounter);
        }
    }
}
