﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;


public class controlOnOffPitch : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform mousePos, targetPos;
    Transform handPos;
    AudioSource a0, aPseudo;
    private float timer = 0.0f;

    public Transform pseudoTarget;

    //public GameObject experimentController1, experimentController2;
    float volumeLimiter = 0.3f;

    public Text AudioStats;

    public int fixedHeight = 0; //0 = lowest 
                                //1 = low height
                                //2 = middle height
                                //3 = high height
                                //4 = highest 


    public int pitchMode = 0;      //0 = HEAD-based RELATIVE pitch
                                   //1 = HEAD-based ABSOLUTE pitch
                                   //2 = HAND-based ABSOLUTE pitch
                                   //3 = MIXED base ABSOLUTE pitch
                                   //4 = HEAD-based FIXED pitch 
                                   //5 = MIXED base RELATIVE pitch 
                                   //6 = MIXED base FIXED pitch
                                   //7 = HAND-based RELATIVE pitch
                                   //8 = HAND-based FIXED pitch

    void Start()
    {

        a0 = targetPos.gameObject.GetComponent<AudioSource>();

        if (pitchMode == 2 || pitchMode == 3 || pitchMode == 5 || pitchMode == 6 || pitchMode == 7 || pitchMode == 8 )
        {
            handPos = null;
            aPseudo = pseudoTarget.gameObject.GetComponent<AudioSource>();
        }
    }

    // Update is called once per frame
    void Update()
    {

        /* if (experimentController1.activeSelf)
         {
             volumeLimiter = experimentController1.GetComponent<experimentController>().userAdjustedVolume;
         }
         else if (experimentController2.activeSelf)
         {
             volumeLimiter = experimentController2.GetComponent<experimentController>().userAdjustedVolume;
         }

         */



        //if head soni, don't do anything, if HAND soni, look for Right_PokePointer(Clone) or Left_PokePointer(Clone) object and use its position instead of the camera position
        //still need to figure out how sound spatiality will work in the hand case, since 3D sound uses the camera position by default


        double distX, distZ, distY;
        double distXZ = 0;
        Vector2 mousePosXZ, targetPosXZ;

        if (a0.isPlaying)
        {

            switch (pitchMode)
            {   
                case 0:                                                                     //HEAD based RELATIVE pitch

                    mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                    targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                    distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                    distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                    distZ = Math.Abs(mousePos.position.z - targetPos.position.z);




                    distY = mousePos.position.y - targetPos.position.y;  // relative  pitch

                    double pitch = 0;

                    if (distY >= 0)                // relative adaptive pitch
                    {
                        pitch = (-Math.Pow(distY, 1.0 / 3) + 1.25f) / 2f;         //cubic "S" function to control pitch (positive height part)
                        volumeLimiter = -1.3684f * (float)pitch + 1.0684f;

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;


                    }
                    else if (distY < 0)
                    {
                        pitch = (Math.Pow(-distY, 1.0 / 3) + 0.85f) / 2f;         //cubic "S" function to control pitch (negative height part)
                        volumeLimiter = 0.3f;
                    }

                    if (pitch < 1.0 && pitch > 0.05)
                    {
                        a0.pitch = (float)pitch;

                    }
                    else if (pitch >= 1.0)
                    {
                        a0.pitch = 1f;
                        volumeLimiter = 0.3f;
                    }
                    else
                    {
                        a0.pitch = 0.05f;
                        volumeLimiter = 1f;
                    }

                    AudioStats.text = "HEAD Relative Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;

                    break;

                case 1:                                                                         //HEAD based ABSOLUTE pitch

                    mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                    targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                    distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                    distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                    distZ = Math.Abs(mousePos.position.z - targetPos.position.z);


                  


                    distY = Math.Abs(mousePos.position.y - targetPos.position.y);       //absolute  pitch
                    a0.pitch = (float)distY + 0.25f;

                    volumeLimiter = -1.3684f * a0.pitch + 1.0684f;

                    if (volumeLimiter < 0.3f)
                        volumeLimiter = 0.3f;
                    else if (volumeLimiter > 1.0f)
                        volumeLimiter = 1f;

                    AudioStats.text = "HEAD Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;
                    break;

                case 2:                                                                         //HAND based ABSOLUTE pitch

                    a0.pitch = 0f;

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand
                        }
                        catch (Exception e2)                                    //if no hands, disable soni
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }

                    
                    if (handPos != null)
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);


                        

                        distY = Math.Abs(handPos.position.y - targetPos.position.y);       //HAND absolute  pitch
                        aPseudo.pitch = (float)distY + 0.1f;

                        volumeLimiter = -1.3684f * aPseudo.pitch + 1.0684f;

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                        AudioStats.text = "HAND Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + aPseudo.pitch;
                    }
                    
                    break;

                case 3:      //MIXED BASE absolute pitch -- whenever hand is not detected, soni is head-based.

                    

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

                        a0.pitch = 0f;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand

                            a0.pitch = 0f;
                        }
                        catch (Exception e2)                                    //if no hands, disable apseudo, enable a0
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            //AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }


                    if (handPos != null)
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);




                        distY = Math.Abs(handPos.position.y - targetPos.position.y);       //HAND absolute  pitch
                        aPseudo.pitch = (float)distY + 0.1f;

                        volumeLimiter = -1.3684f * aPseudo.pitch + 1.0684f;

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                        AudioStats.text = "HAND Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + aPseudo.pitch;
                    }
                    else
                    {
                        mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                        distZ = Math.Abs(mousePos.position.z - targetPos.position.z);





                        distY = Math.Abs(mousePos.position.y - targetPos.position.y);       //HEAD absolute  pitch
                        a0.pitch = (float)distY + 0.25f;

                        volumeLimiter = -1.3684f * a0.pitch + 1.0684f;

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                        AudioStats.text = "HEAD Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;
                    }

                    break;

                case 4:     //fixed pitch

                    mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                    targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                    distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                    distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                    distZ = Math.Abs(mousePos.position.z - targetPos.position.z);


                    if (fixedHeight == 0)           //attach these to different notes   do C / re D / mi E / fa F / sol G / la A / si B    
                    { 
                        a0.pitch = 0.2469f;  // si  B3 
                        volumeLimiter = 1f;
                    }
                    else if (fixedHeight == 1)
                    {
                        a0.pitch = 0.3296f;  // mi  E4
                        volumeLimiter = 1f;
                    }
                    else if (fixedHeight == 2)
                    {
                        a0.pitch = 0.5233f;    // do  C5 
                        volumeLimiter = 0.9f;
                    }
                    else if (fixedHeight == 3)
                    {
                        a0.pitch = 0.88f;        // la  A5      
                        volumeLimiter = 0.8f;
                    }
                    else if (fixedHeight == 4)
                    {
                        a0.pitch = 1.175f;        // re  D6
                        volumeLimiter = 0.5f;
                    }
                    else
                        a0.pitch = 0f;

                    AudioStats.text = "HEAD Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = not relevant\n pitch = " + a0.pitch;

                    break;

                

                case 5:      //MIXED BASE relative pitch -- whenever hand is not detected, soni is head-based.

                    pitch = 0;

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

                        a0.pitch = 0f;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand

                            a0.pitch = 0f;
                        }
                        catch (Exception e2)                                    //if no hands, disable apseudo, enable a0
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            //AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }


                    if (handPos != null)                // HAND relative
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);





                        //////////////////////////////////
                        distY = handPos.position.y - targetPos.position.y;  // relative  pitch



                      

                        if (distY >= 0)                // relative adaptive pitch
                        {
                            pitch = (-Math.Pow(distY, 1.0 / 3) + 1.25f) / 2f;         //cubic "S" function to control pitch (positive height part)
                            volumeLimiter = -1.3684f * (float)pitch + 1.0684f;

                            if (volumeLimiter < 0.3f)
                                volumeLimiter = 0.3f;


                        }
                        else if (distY < 0)
                        {
                            pitch = (Math.Pow(-distY, 1.0 / 3) + 0.85f) / 2f;         //cubic "S" function to control pitch (negative height part)
                            volumeLimiter = 0.3f;
                        }

                        if (pitch < 1.0 && pitch > 0.05)
                        {
                            aPseudo.pitch = (float)pitch;

                        }
                        else if (pitch >= 1.0)
                        {
                            aPseudo.pitch = 1f;
                            volumeLimiter = 0.3f;
                        }
                        else
                        {
                            aPseudo.pitch = 0.05f;
                            volumeLimiter = 1f;
                        }

                        AudioStats.text = "HAND Relative Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + aPseudo.pitch;

                        //////////////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                       
                    }
                    else                                                            //HEAD relative  pitch
                    {
                        mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                        distZ = Math.Abs(mousePos.position.z - targetPos.position.z);





                        distY = mousePos.position.y - targetPos.position.y;       
                        //a0.pitch = (float)distY + 0.25f;

                        //volumeLimiter = -1.3684f * a0.pitch + 1.0684f;

                        ////////////////////////
                        if (distY >= 0)                // relative adaptive pitch
                        {
                            pitch = (-Math.Pow(distY, 1.0 / 3) + 1.25f) / 2f;         //cubic "S" function to control pitch (positive height part)
                            volumeLimiter = -1.3684f * (float)pitch + 1.0684f;

                            if (volumeLimiter < 0.3f)
                                volumeLimiter = 0.3f;


                        }
                        else if (distY < 0)
                        {
                            pitch = (Math.Pow(-distY, 1.0 / 3) + 0.85f) / 2f;         //cubic "S" function to control pitch (negative height part)
                            volumeLimiter = 0.3f;
                        }

                        if (pitch < 1.0 && pitch > 0.05)
                        {
                            a0.pitch = (float)pitch;

                        }
                        else if (pitch >= 1.0)
                        {
                            a0.pitch = 1f;
                            volumeLimiter = 0.3f;
                        }
                        else
                        {
                            a0.pitch = 0.05f;
                            volumeLimiter = 1f;
                        }

                        AudioStats.text = "HEAD Relative Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;

                        ////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                        //AudioStats.text = "HEAD Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;
                    }

                    break;


                case 6:      //MIXED base FIXED pitch -- whenever hand is not detected, soni is head-based.

                    pitch = 0;

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

                        a0.pitch = 0f;
                        //a0.volume = 0f;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand

                            a0.pitch = 0f;
                            //a0.volume = 0f;
                        }
                        catch (Exception e2)                                    //if no hands, disable apseudo, enable a0
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            //aPseudo.volume = 0f;
                            //AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }


                    if (handPos != null)                // HAND relative
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);





                        //////////////////////////////////
                        if (fixedHeight == 0)           //attach these to different notes   do C / re D / mi E / fa F / sol G / la A / si B    
                        { 
                            aPseudo.pitch = 0.2469f;  // si  B3 
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 1)
                        {
                            aPseudo.pitch = 0.3296f;  // mi  E4
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 2)
                        {
                            aPseudo.pitch = 0.5233f;    // do  C5 
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 3)
                        {
                            aPseudo.pitch = 0.88f;        // la  A5      
                            volumeLimiter = 0.3f;
                        }
                        else if (fixedHeight == 4)
                        {
                            aPseudo.pitch = 1.175f;        // re  D6
                            volumeLimiter = 0.3f;
                        }
                        else
                            aPseudo.pitch = 0f;

                        AudioStats.text = "HAND FIXED Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = not relevant\n pitch = " + aPseudo.pitch;


                        //////////////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                       
                    }
                    else                                                            //HEAD FIXED  pitch
                    {
                        mousePosXZ = new Vector2(mousePos.position.x, mousePos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(mousePos.position.x - targetPos.position.x);
                        distZ = Math.Abs(mousePos.position.z - targetPos.position.z);

                        ////////////////////////
                        if (fixedHeight == 0)           //attach these to different notes   do C / re D / mi E / fa F / sol G / la A / si B    
                        { 
                            a0.pitch = 0.2469f;  // si  B3 
                            volumeLimiter = 1f;
                        }
                        else if (fixedHeight == 1)
                        {
                            a0.pitch = 0.3296f;  // mi  E4
                            volumeLimiter = 1f;
                        }
                        else if (fixedHeight == 2)
                        {
                            a0.pitch = 0.5233f;    // do  C5 
                            volumeLimiter = 0.9f;
                        }
                        else if (fixedHeight == 3)
                        {
                            a0.pitch = 0.88f;        // la  A5      
                            volumeLimiter = 0.8f;
                        }
                        else if (fixedHeight == 4)
                        {
                            a0.pitch = 1.175f;        // re  D6
                            volumeLimiter = 0.5f;
                        }
                        else
                            a0.pitch = 0f;

                        AudioStats.text = "HEAD FIXED Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = not relevant\n pitch = " + a0.pitch;

                        ////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                        //AudioStats.text = "HEAD Absolute Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;
                    }

                    break;

                case 7:      //HAND base RELATIVE pitch -- whenever hand is not detected, soni is OFF.

                    pitch = 0;

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

                        a0.pitch = 0f;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand

                            a0.pitch = 0f;
                        }
                        catch (Exception e2)                                    //if no hands, disable apseudo, enable a0
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            //AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }


                    if (handPos != null)                // HAND relative
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);





                        //////////////////////////////////
                        distY = handPos.position.y - targetPos.position.y;  // relative  pitch



                      

                        if (distY >= 0)                // relative adaptive pitch
                        {
                            pitch = (-Math.Pow(distY, 1.0 / 3) + 1.25f) / 2f;         //cubic "S" function to control pitch (positive height part)
                            volumeLimiter = -1.3684f * (float)pitch + 1.0684f;

                            if (volumeLimiter < 0.3f)
                                volumeLimiter = 0.3f;


                        }
                        else if (distY < 0)
                        {
                            pitch = (Math.Pow(-distY, 1.0 / 3) + 0.85f) / 2f;         //cubic "S" function to control pitch (negative height part)
                            volumeLimiter = 0.3f;
                        }

                        if (pitch < 1.0 && pitch > 0.05)
                        {
                            aPseudo.pitch = (float)pitch;

                        }
                        else if (pitch >= 1.0)
                        {
                            aPseudo.pitch = 1f;
                            volumeLimiter = 0.3f;
                        }
                        else
                        {
                            aPseudo.pitch = 0.05f;
                            volumeLimiter = 1f;
                        }

                        AudioStats.text = "HAND Relative Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + aPseudo.pitch;

                        //////////////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                       
                    }


                    break;

                case 8:      //HAND base FIXED pitch -- whenever hand is not detected, soni is OFF.

                    pitch = 0;

                    if (!aPseudo.isPlaying)
                        aPseudo.Play();

                    try
                    {
                        handPos = GameObject.Find("Right_PokePointer(Clone)").transform;   //look for right hand

                        a0.pitch = 0f;
                        //a0.volume = 0f;
                    }
                    catch (Exception e)
                    {
                        try
                        {
                            handPos = GameObject.Find("Left_PokePointer(Clone)").transform;    //if no right hand, look for left hand

                            a0.pitch = 0f;
                            //a0.volume = 0f;
                        }
                        catch (Exception e2)                                    //if no hands, disable apseudo, enable a0
                        {
                            handPos = null;

                            aPseudo.pitch = 0f;
                            //aPseudo.volume = 0f;
                            //AudioStats.text = "HAND Absolute Pitch \n NO HANDS FOUND";
                        }
                    }


                    if (handPos != null)                // HAND relative
                    {

                        Vector3 newPos;

                        newPos.x = targetPos.position.x + mousePos.position.x - handPos.position.x;
                        newPos.y = targetPos.position.y + mousePos.position.y - handPos.position.y;
                        newPos.z = targetPos.position.z + mousePos.position.z - handPos.position.z;

                        pseudoTarget.position = newPos;

                        mousePosXZ = new Vector2(handPos.position.x, handPos.position.z);
                        targetPosXZ = new Vector2(targetPos.position.x, targetPos.position.z);

                        distXZ = Vector2.Distance(mousePosXZ, targetPosXZ);


                        distX = Math.Abs(handPos.position.x - targetPos.position.x);
                        distZ = Math.Abs(handPos.position.z - targetPos.position.z);





                        //////////////////////////////////
                        if (fixedHeight == 0)           //attach these to different notes   do C / re D / mi E / fa F / sol G / la A / si B    
                        { 
                            aPseudo.pitch = 0.2469f;  // si  B3 
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 1)
                        {
                            aPseudo.pitch = 0.3296f;  // mi  E4
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 2)
                        {
                            aPseudo.pitch = 0.5233f;    // do  C5 
                            volumeLimiter = 0.4f;
                        }
                        else if (fixedHeight == 3)
                        {
                            aPseudo.pitch = 0.88f;        // la  A5      
                            volumeLimiter = 0.3f;
                        }
                        else if (fixedHeight == 4)
                        {
                            aPseudo.pitch = 1.175f;        // re  D6
                            volumeLimiter = 0.3f;
                        }
                        else
                            aPseudo.pitch = 0f;

                        AudioStats.text = "HAND FIXED Pitch \n distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = not relevant\n pitch = " + aPseudo.pitch;


                        //////////////////////////////////

                        if (volumeLimiter < 0.3f)
                            volumeLimiter = 0.3f;
                        else if (volumeLimiter > 1.0f)
                            volumeLimiter = 1f;

                       
                    }

                    break;

                default:
                    distY = mousePos.position.y - targetPos.position.y;  // head relative pitch as default
                    break;
            }



            //Debug.Log("distXZ = " + distXZ + "height = " + distY);


            //AudioStats.text = "distXZ = " + distXZ + "\n distX = " + distX + "\n distZ = " + distZ + "\n height = " + distY + "\n pitch = " + a0.pitch;






            //Debug.Log("real pitch = " + a0.pitch);


            //sound alternation according to XZ dist:

            if ( (pitchMode == 2 ||  pitchMode == 7 || pitchMode == 8) && handPos == null)    //if no hands found, disable soni in hand-based modes
            {
                aPseudo.volume = 0f;
                a0.volume = 0f;
                   
            }
            else 
            { 

                timer += Time.deltaTime;



                if (distXZ <= 0.04)
                { 
                    a0.volume = volumeLimiter;

                    if (pitchMode != 0 &&  pitchMode != 1 && pitchMode != 4 )
                        aPseudo.volume = volumeLimiter;
                }
                else if (a0.volume == 0 && timer > 0.1f)
                {
                    timer = 0;
                    a0.volume = volumeLimiter;
                    //a0.volume = 1f;

                    if (pitchMode != 0 &&  pitchMode != 1 && pitchMode != 4 )
                        aPseudo.volume = volumeLimiter;
                    //aPseudo.volume = 1f;
                }

                else if (timer > distXZ / 6f + 0.04f)
                {
                    timer = 0;


                    if (a0.volume > 0f)
                    { 
                        a0.volume = 0f;
                        
                        if (pitchMode != 0 &&  pitchMode != 1 && pitchMode != 4 )
                            aPseudo.volume = 0f;
                    }
                    else
                    { 
                        a0.volume = volumeLimiter;
                        
                        if (pitchMode != 0 &&  pitchMode != 1 && pitchMode != 4 )
                            aPseudo.volume = volumeLimiter;
                    }
                }
            }
        }
    }
}
